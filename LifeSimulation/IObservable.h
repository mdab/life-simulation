#pragma once
class IObservable
{
public:

	virtual ~IObservable() {}

	virtual void act() = 0;
	virtual void render() = 0;
};

