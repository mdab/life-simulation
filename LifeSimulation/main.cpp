// LifeSimulation.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int main()
{

	srand(time(NULL));

	//
	int width, height;
	cout << "Map width: ";
	cin >> width;

	cout << "Map height: ";
	cin >> height;

	int preysNum;
	cout << "Number of Preys: ";
	cin >> preysNum;

	int predatorsNum;
	cout << "Number Predators: ";
	cin >> predatorsNum;

	int teleportsNum;
	cout << "Number Teleports: ";
	cin >> teleportsNum;
		
	int stepsNumber;
	cout << "Number of steps: ";
	cin >> stepsNumber;
	
	Map * map = new Map(width, height);
	
	for (int i = 0; i < teleportsNum; i++)
	{
		int x = rand() % map->getWidth();
		int y = rand() % map->getHeight();

		Teleporter * teleporter = new Teleporter(x, y, 'O', map);
	}

	for (int i = 0; i < preysNum; i++)
	{
		int x = rand() %  map->getWidth();
		int y = rand() %  map->getHeight();
			
		Prey * prey = new Prey(x, y, PREY, '+', map, 10);
		
	}

	for (int i = 0; i < predatorsNum; i++)
	{
		int x = rand() % map->getWidth();
		int y = rand() % map->getHeight();
		int iq = rand() % 9;

		Predator * predator = new Predator(x, y, PREDATOR, 'X', map, 10, iq);

	}

	map->update(stepsNumber);

	system("PAUSE");


	return 0;
	}




