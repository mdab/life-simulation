#include "stdafx.h"
#include "Map.h"


Map::Map(int width, int height) : width(width), height(height)
{
	fields.resize(width, height);

	GameObject::mapWidth = width;
	GameObject::mapHeight = height;
}

Map::~Map()
{
}

void Map::update(int steps)
{
	render();

	for (int i = 0; i < steps; i++)
	{
		for (Organism * organism : organisms)
		{	
			if (organism->getAlive()) {
				organism->act();
			}
		}
		organismsGarbageCollector();
	}

	//po turze trzeba wyrzucic wszystkie zabite zwierzeta z listy

}

void Map::render()
{
	system("CLS");
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			fields.get(j, i)->render();
		}

		cout << endl;
	}
	cout << endl << endl;
	for (Organism* organism : organisms)
	{
		cout << organism->getX() << organism->getY() <<" "<< organism->getLifePoints()<<endl;

	}

	
	system("PAUSE");
}

int Map::getWidth()
{
	return width;
}

int Map::getHeight()
{
	return height;
}

Matrix<Field> Map::getFields()
{
	return fields;
}

list<Organism*> * Map::getOrganisms()
{
	return &organisms;
}

void Map::addOrganism(Organism * organism)
{
	organisms.push_back(organism);
}

void Map::moveOrganism(Organism * organism, Coordinate move)
{
	fields.get(organism->getX(), organism->getY())->removeOrganism(organism);
	
	organism->setX(move.x);
	organism->setY(move.y);

	fields.get(organism->getX(), organism->getY())->addOrganism(organism);

	render();
}

void Map::killOrganism(Organism * organism)
{
	fields.get(organism->getX(), organism->getY())->removeOrganism(organism);

	organism->kill();

	render();

}

void Map::organismsGarbageCollector()
{

	list <Organism*>::iterator organism = organisms.begin();
	while (organism != organisms.end())
	{
		if (!(*organism)->getAlive())
		{
			delete (*organism);
			
			organism = organisms.erase(organism);
			
		}
		else {
			++organism;
		}

	}

	
	
}

Organism* Map::seekOrganism(Coordinate field, Race race)
{
	list <Organism*> * fieldOrganismList = fields.get(field.x, field.y)->getFieldOrganisms();

	for (Organism * organism: *fieldOrganismList)
	{
		if (organism->getRace() == race && organism->getAlive())  
		{
			return organism;
		}
	}

	return nullptr;
}

void Map::addOrganismOnMap(Organism * organism)
{
	fields.get(organism->getX(), organism->getY())->addOrganism(organism);
}

void Map::addTeleporterOnMap(Teleporter * teleporter)
{
	fields.get(teleporter->getX(), teleporter->getY())->addTeleporter(teleporter);
}
