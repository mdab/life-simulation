#pragma once
#include "NonCopyable.h"
#include "GameObject.h"
#include "IObservable.h"

class GameObject;

class Field final : /*public IObservable, */public NonCopyable<Field> //metoda act w Fieldzie?
{
public:
	Field();
	virtual ~Field();

	/*void update();*/
	void render();


	char getSprite();

	list<Organism*> * getFieldOrganisms();
	
	void addOrganism(Organism * organism);
	void removeOrganism(Organism * organism);

	void addTeleporter(Teleporter * teleporter);

	void eventsHandler(Organism * organism);
	
	int eatFood();

private:
	char sprite = '.';
	list<Organism*> fieldOrganisms;

	Teleporter * teleporter;

	int food;

};

