#include "stdafx.h"
#include "GameObject.h"


int GameObject::mapWidth;
int GameObject::mapHeight;

GameObject::GameObject(unsigned int x, unsigned int y, char sprite, Map * map) : x(x), y(y), sprite(sprite), map(map)
{
	//this -> map = map;
	generateId();
}


GameObject::~GameObject()
{
}

void GameObject::act()
{
	/*x = rand() % GameObject::mapWidth;
	y = rand() % GameObject::mapHeight;*/

	//czemu nie na poziomie Mapy?
}

void GameObject::render()
{
	cout << getSprite();
}



int GameObject::getId()
{
	return id;
}


int GameObject::getX()
{
	return x;
}

void GameObject::setX(unsigned int x)
{
	this->x = x;
}


int GameObject::getY()
{
	return y;
}

void GameObject::setY(unsigned int y)
{
	this->y = y;
}


char GameObject::getSprite()
{
	return sprite;
}


void GameObject::setSprite(char sprite)
{
	this->sprite = sprite;
}


Field * GameObject::getField()
{
	return field;
}

void GameObject::setField(Field * field)
{
	this->field = field;
}

bool GameObject::getAlive()
{
	return alive;
}

void GameObject::kill()
{
	alive = false;
}





void GameObject::generateId()
{
	static int _nextId;
	id = _nextId;
	_nextId++;
}


int GameObject::findDistance(GameObject * gameObject)
{
	int myX, myY;
	myX = this->getX();
	myY = this->getY();

	int targetX, targetY;
	targetX = gameObject->getX();
	targetY = gameObject->getY();

	return sqrt((myX - targetX)*(myX - targetX) + (myY - targetY)*(myY - targetY));
}

bool GameObject::collisionWith(GameObject * gameObject)
{
	if (this->getX() == gameObject->getX() && this->getY() == gameObject->getY()) {
		return true;
	}
	else {
		return false;
	}
}
