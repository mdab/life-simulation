#pragma once
#include "stdafx.h"


class Organism;

class Teleporter:
	public GameObject
{
public:
	Teleporter(unsigned int x, unsigned int y, char sprite, Map * map);
	~Teleporter();

	void teleport(Organism * organism);
	
};

