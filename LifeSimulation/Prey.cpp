#include "stdafx.h"
#include "Prey.h"

Prey::Prey(unsigned int x, unsigned int y, Race rc, char sprite, Map * map, unsigned int lp) :
	Organism(x, y, rc, sprite, map, lp)
{
}


Prey::~Prey()
{
}

void Prey::act()
{
	moveOrganism();
	eat();
}

void Prey::moveOrganism() {

	initializeMoveBuffer();

	if (!moveBuffer.empty()) {

		Coordinate move = moveBuffer[rand() % moveBuffer.size()];

		map->moveOrganism(this, move);
				
	}

	moveBuffer.clear();

	
}

void Prey::eat()
{
	int eatenFood = this->getField()->eatFood();

	if (eatenFood==0)
	{
		this->changeLifePoints(-1);
		if (this->getLifePoints()==0)
		{
			map->killOrganism(this);
		}
	}
	else {
		this->changeLifePoints(eatenFood);
	}
	
}


void Prey::initializeMoveBuffer() {
	
	Coordinate move(0,0);

	for (int i = -1; i <= 1; i++)
	{
		if (i != 0) {
			move.x = i;
			move.y = 0;
			if (checkXY(move))
			{
				move.x += this->getX();
				move.y += this->getY();
				moveBuffer.push_back(move);

			}
		}
		else {
			for (int j = -1; j <= 1; j++)
			{
				if (j != 0) {
					move.x = 0;
					move.y = j;

					if (checkXY(move))
					{
						move.x += this->getX();
						move.y += this->getY();
						moveBuffer.push_back(move);
					}

				}
			}
		}

	}

	//random_shuffle(moveBuffer.begin(), moveBuffer.end());



}
