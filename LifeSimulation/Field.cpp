#include "stdafx.h"
#include "Field.h"

Field::Field()
{
	this->teleporter = NULL;
	this->food = rand() % 2;
}


Field::~Field()
{
}


void Field::render()
{
	if (fieldOrganisms.empty()) {
		
		if (teleporter != NULL) {
			cout<<teleporter->getSprite();
		}
		else {
			cout << getSprite();
		}
	}
	else {
		cout << fieldOrganisms.back()->getSprite();
	}

	//zdecydowac jak to sie ma wyswietlac
}



char Field::getSprite()
{
	return sprite;
}


list<Organism*> * Field::getFieldOrganisms()
{
	return &fieldOrganisms;
}

void Field::addOrganism(Organism * organism)
{
	organism->setField(this);
	fieldOrganisms.push_back(organism);
	eventsHandler(organism);

}

void Field::removeOrganism(Organism * organism)
{
	fieldOrganisms.remove(organism);
}

void Field::addTeleporter(Teleporter * teleporter)
{
	this->teleporter = teleporter;	
}

void Field::eventsHandler(Organism * organism)
{
	if (teleporter != nullptr) {
		teleporter->teleport(organism);
	}
	else {
		if (!fieldOrganisms.empty() && organism->getRace()==PREY) {

			for (Organism * org : fieldOrganisms)
			{
				if (org->getRace() == PREDATOR)
				{
					org->eat(organism);
					break;
				}

			}
		}
	}
}

int Field::eatFood()
{
	int eatenFood = food;
	food = 0;
	return eatenFood;
}

//dodac eventy jedzenia i teleportowania